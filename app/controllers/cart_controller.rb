class CartController < ApplicationController
  def index
    @products = Product.all
  end

  def calculate
    cart = Cart.new params[:products]
    response = {
      discount_sum: cart.discount_sum,
      total_sum: cart.total_sum
    }
    render json: response
  end
end
