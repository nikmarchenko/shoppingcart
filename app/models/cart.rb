class Cart
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def products
    @products ||= begin
      return [] unless params
      Product.includes(:offer)
        .where(id: params.map { |item| item['id'] })
    end
  end

  def offers
    @offers ||= products.map(&:offer).compact
  end

  def quantity(product_id)
    quantity = params.select { |item| item['id'].to_i == product_id }
    quantity.first['count'].to_i
  end

  def overall_sum
    @overall_sum ||= begin
      products.to_a.sum { |product| product.price * quantity(product.id) }
    end
  end

  def discount_sum
    @discount_sum ||= begin
      return 0 if offers.blank?

      offers.to_a.sum do |offer|
        offer.get_discount \
          count: quantity(offer.product_id),
          overall_sum: overall_sum
      end
    end
  end

  def total_sum
    overall_sum - discount_sum
  end
end
