class Offer < ActiveRecord::Base
  belongs_to :product
  store_accessor :properties, :product_count, :discount, :type

  def get_discount(count:, overall_sum:)
    return 0 if count < product_count

    case type
    when 'percentage'
      discount * overall_sum / 100
    when 'cash'
      discount
    end
  end
end
