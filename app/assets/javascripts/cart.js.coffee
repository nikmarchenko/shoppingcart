class Cart
  constructor: ->
    @addProduct()
    @changeQuantity()

  addProduct: ->
    cart = this

    $(document).on 'click', 'td.name', ->
      container = $(this).closest('tr')
      container.toggleClass('info')
      container.find('input').prop 'disabled', !container.hasClass('info')

      cart.calculatePrice()

  changeQuantity: ->
    cart = this
    $(document).on 'change', '.quantity', ->
      cart.calculatePrice()


  calculatePrice: ->
    form = $('#cart-form')

    $.ajax
      type: 'POST'
      url: form.attr('action')
      data: form.serialize()
      success: (data) ->
        $('#discount').html(data.discount_sum)
        $('#total').html(data.total_sum)


$ -> new Cart

