describe Cart do
  let!(:product1) { Product.create! name: 'Gingerbread Black Tea', price: 100 }
  let!(:product2) { Product.create! name: 'Earl Grey Black Tea',   price: 50 }
  let!(:product3) { Product.create! name: 'Cacao Mint Black Tea',  price: 70 }

  let!(:offer1) do
    Offer.create! name: 'Get 50% discount',
      properties: { product_count: 2, discount: 50, type: 'percentage' }
  end

  let!(:offer2) do
    Offer.create! name: 'Get 30$ discount',
      properties: { product_count: 3, discount: 30, type: 'cash' }
  end

  let(:cart) do
    Cart.new [{"id"=>product1.id, "count"=>"2"}, {"id"=>product2.id, "count"=>"3"}]
  end

  before do
    offer1.update! product_id: product1.id
    offer2.update! product_id: product2.id
  end

  describe '#overall_sum' do
    it 'calculates overall sum correctly' do
      expect(cart.overall_sum).to eq(350)
    end
  end

  describe '#discount_sum' do
    context 'when all offers available' do
      it 'calculates discount sum correctly' do
        expect(cart.discount_sum).to eq(205)
      end
    end

    context 'when condition for the second offer not complete' do
      before { offer2.update! product_count: 5 }
      it 'calculates discount sum correctly' do
        expect(cart.discount_sum).to eq(175)
      end
    end
  end

  describe '#total_sum' do
    it 'calculates total sum correctly' do
      expect(cart.total_sum).to eq(145)
    end
  end
end
