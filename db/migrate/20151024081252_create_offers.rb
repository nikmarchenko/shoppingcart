class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string  :name
      t.json    :properties
      t.integer :product_id

      t.timestamps null: false
    end
  end
end
