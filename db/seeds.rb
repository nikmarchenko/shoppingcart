Product.delete_all
Offer.delete_all

Product.create! name: 'Gingerbread Black Tea', price: 100
Product.create! name: 'Earl Grey Black Tea',   price: 50
Product.create! name: 'Cacao Mint Black Tea',  price: 70

Offer.create! name: 'Get 50% discount',
  properties: { product_count: 2, discount: 50, type: 'percentage' }

Offer.create! name: 'Get 30$ discount',
  properties: { product_count: 3, discount: 30, type: 'cash' }

Offer.first.update! product_id: Product.first.id
Offer.last.update!  product_id: Product.second.id
