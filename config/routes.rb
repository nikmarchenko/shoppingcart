Rails.application.routes.draw do
  root 'cart#index'
  post '/calculate', to: 'cart#calculate'
end
